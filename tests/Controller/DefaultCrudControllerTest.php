<?php

namespace App\Tests\Controller;
use PhpParser\PrettyPrinter\Standard;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class DefaultControllerTest
 *
 * @package App\Tests\Controller
 */

class DefaultCrudControllerTest extends WebTestCase
{
    public function testIndexAction()
    {
        $client = static :: createClient();
        $client->request('GET', '/article'); //composer req symfony/browser-kit
        $this->assertEquals(200, $client->getResponse()->getStatusCode()); //composer require phpunit/phpunit
    }

    public function testCreateAction()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/article/create');
        $button = $crawler->selectButton('article[submit]');
        $form = $button->form([
            'article[name]'=>'create_test',
            'article[description]'=>'yes_created',
            'article[created_at][date][day]'=>'30',
            'article[created_at][date][month]'=>'3',
            'article[created_at][date][year]'=>'2021',
            'article[created_at][time][hour]'=>'4',
            'article[created_at][time][minute]'=>'19'
        ]);
        $client->submit($form);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());

    }

    public function testEditAction()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/article/edit/29');
        $button = $crawler->selectButton('article[submit]');
        $form = $button->form([
            'article[name]'=>'test_edit',
            'article[description]'=>'yes_edits',
            'article[created_at][date][day]'=>'27',
            'article[created_at][date][month]'=>'11',
            'article[created_at][date][year]'=> '2022',
            'article[created_at][time][hour]'=>'2',
            'article[created_at][time][minute]'=>'40',
        ]);

        $client->submit($form);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    public function testDeleteAction()
    {
        $client = static ::createClient();
        $client->request('GET', '/article/delete/29');
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }
}



?>